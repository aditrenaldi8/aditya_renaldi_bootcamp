import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.css']
})
export class CourseListComponent implements OnInit {

  data: Object[] ;
  constructor(private service:DataService) { }

  ngOnInit() {
    this.data = this.service.data;
  }

  course: string="";
  price: number = 0;
  teacher: string="";
  
  add(){
    let id = 1;
    if(this.data.length > 0){
      id = this.data[this.data.length-1]["id"] +1;
    }

    this.data.push({
      'id': id, "course_name": this.course, "price":this.price , "teacher":this.teacher},
    );

    this.course = "";
    this.price = 0;
    this.teacher ="";
  } 
  
  reset(){
    this.course = "";
    this.price = 0;
    this.teacher ="";
  }

}

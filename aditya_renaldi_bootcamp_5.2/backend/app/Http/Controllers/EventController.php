<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Event;
use App\Ticket;

class EventController extends Controller
{
    public function getEvent(){
        $list = Event::get();

         return response()->json($list, 201);
    }

    public function buyTicket(Request $request){
        DB::beginTransaction();

        try{
             $this->validate($request,[
                'id' => 'required',
                'name' => 'required',
                'phone' => 'required',
                'email' => 'required',
            ]);

            $event = Event::find($request->input('id'));
            if( $event->available_ticket > 0){
                $event->available_ticket = $event->available_ticket - 1;
            
                $event->save();

                $newData = new Ticket;
                $newData->ticket_number = $event->event_code." - ". ($event->total_seat - $event->available_ticket);
                $newData->buyer_name = $request->input('name');
                $newData->buyer_phone = $request->input('phone');
                $newData->buyer_email = $request->input('email');
                $newData->event_id = $request->input('id');
                $newData->save();

                $list = Event::get();
                DB::commit();
                return response()->json($list, 201);
            }else{
                $list = Event::get();
                return response()->json($list, 201);
            }

        }catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message"=> $e->getMessage ], 500);
        }
     
    }
    
    public function addEvent(Request $request){
        DB::beginTransaction();

        try{
            $this->validate($request,[
                'event_code' => 'required',
                'event_name' => 'required',
                'event_date_time' => 'required',
                'place' => 'required',
                'event_price' => 'required',
                'total_seat' => 'required',
            ]);


            $newData = new Event;
            $newData->event_code = $request->input('event_code');
            $newData->event_name = $request->input('event_name');
            $newData->event_date_time = $request->input('event_date_time');
            $newData->place = $request->input('place');
            $newData->event_price = $request->input('event_price');
            $newData->total_seat = $request->input('total_seat');
            $newData->available_ticket = $request->input('total_seat');
            $newData->save();

            $list = Event::get();

            DB::commit();
            return response()->json($list, 201);

        }catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message"=> $e->getMessage ], 500);
        }
    }

    public function getData(Request $request){
       try{
            $this->validate($request,[
                'id' => 'required',
            ]);

            $list = Event::find($request->input('id'));

            return response()->json($list, 200);

        }catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message"=> $e->getMessage ], 500);
        }


    }
}

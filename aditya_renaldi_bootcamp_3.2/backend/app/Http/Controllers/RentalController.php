<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illiminate\Support\Facades\DB;

use App\Rental;
use App\Room;

class RentalController extends Controller
{
    
    public function BookRoom(){
        DB::beginTransaction();

        try{
            $this->validate($request,[
                    'customer_id' => 'required',
                    'room_id' => 'required',
        ]);

            $newData = new Rental;
            $newData->customer_id = $request->customer_id;
            $newData->room_id = $request->room_id;
            $newData->check_in = date("Y-m-d h:i:s");
            $newData->save();

            //update status kamar menjadi reserved
            $room = Room::find($request->id);
            $room->status = true;
            $room->save();

        DB::commit();
        return "Data Berhasil Ditambahkan";

        }catch(\Exception $e){
            DB::rollBack();
            return ($e->getMessage);
        }
    }

}

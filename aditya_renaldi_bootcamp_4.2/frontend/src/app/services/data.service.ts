import { Injectable } from '@angular/core';

@Injectable()
export class DataService {

  constructor() { }

  data:object[] = [
    {'id': 1, "course_name": "Programming Fundamental SC", "price":2300000 , "teacher":"Mr. Josh"},
    {'id': 2, "course_name": "Backend Development SC", "price":2400000 , "teacher":"Mr. John Doe"},
    {'id': 3, "course_name": "Frontend Development SC", "price":2500000 , "teacher":"Mr. Khan"},
    {'id': 4, "course_name": "GoLang Developer FS", "price":2600000 , "teacher":"Mr. Steve"},
    {'id': 5, "course_name": "Mobile Developer FS", "price":2700000 , "teacher":"Mr. George"},
  ] ;

}

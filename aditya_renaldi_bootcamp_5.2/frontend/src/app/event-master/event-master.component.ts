import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-event-master',
  templateUrl: './event-master.component.html',
  styleUrls: ['./event-master.component.css']
})
export class EventMasterComponent implements OnInit {

  constructor(private api:DataService) { }

  code:string= "";
  name:string= "";
  time:string= "";
  location:string="";
  price:string="";
  total:string="";

  event:boolean = false;

  event2:boolean = true;

  ngOnInit() {

  }

  show(){
    this.event = true;
    this.event2 = false;
  }

  show2(){
    this.event = false;
    this.event2 = true;
  }


  add(){
    this.api.addData({
      "event_code" : this.code,
      "event_name" : this.name,
      "event_date_time" : this.time,
      "place" : this.location,
      "event_price" : this.price,
      "total_seat" : this.total
    }).subscribe(result => this.api.data = result);

  this.code = "";
  this.name = "";
  this.time = "";
  this.location ="";
  this.price ="";
  this.total ="";
  }

}

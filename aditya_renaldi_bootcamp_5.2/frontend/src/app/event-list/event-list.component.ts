import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.css']
})
export class EventListComponent implements OnInit {

  id : string = "";
  name : string = '';
  phone : string = '';
  email : string = '';

  detail:object[];

  ticket:boolean = false;

  constructor(private api: DataService) { 
    
  }

  ngOnInit() {
    this.api.getData()
    .subscribe(result => this.api.data = result);
  }

  buy(){
    this.api.buyTicket({
      "id" : this.id,
      "name" : this.name,
      "phone" : this.phone,
      "email" : this.email
    }).subscribe(result => this.api.data = result);

    this.name="";
    this.phone="";
    this.email="";
    this.id="";

     this.ticket = false;
  }

  show(id){
    this.ticket = true;
    this.id = id;
    this.api.getDetail({
      "id" : this.id
    }).subscribe(result => this.detail = result);
  }
  

}

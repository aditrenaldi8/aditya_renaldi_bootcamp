<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Student;

class StudentController extends Controller
{
    public function add(Request $request){
        DB::beginTransaction();

        try{
             $this->validate($request,[
                'first_name' => 'required',
                'last_name' => 'required',
                'address' => 'required',
                'phone' => 'required',
                'email' => 'required',
            ]);

            if(isset($request->id)){

                $data = Student::find($request->id);
                $data->first_name = $request->input('first_name');
                $data->last_name = $request->input('last_name');
                $data->address = $request->input('address');
                $data->phone = $request->input('phone');
                $data->email = $request->input('email');
                $data->save();

                $list = Student::find($request->id);

            }else{

                $newData = new Student;
                $newData->first_name = $request->input('first_name');
                $newData->last_name = $request->input('last_name');
                $newData->address = $request->input('address');
                $newData->phone = $request->input('phone');
                $newData->email = $request->input('email');
                $newData->save();

                $list = Student::orderBy('id', 'desc')->first();
            }


            DB::commit();
            return response()->json($list, 201);

        }catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message"=> $e->getMessage ], 500);
        }
        
    }
}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-room-list',
  templateUrl: './room-list.component.html',
  styleUrls: ['./room-list.component.css']
})
export class RoomListComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  data:object[] = [
    {'id': '1', "room_number": "102", "price":2300000 , "status": 1},
    {'id': '2', "room_number": "202", "price":2400000 , "status": 0},
    {'id': '3', "room_number": "302", "price":2500000 , "status": 0},
    {'id': '4', "room_number": "402", "price":2600000 , "status": 1},
    {'id': '5', "room_number": "502", "price":2700000 , "status": 0},
  ] ;

  book(index){
    this.data[index]["status"] = 1;
  }

}

import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch'; 


@Injectable()
export class DataService {

  constructor(private http:Http) { }

  data:object[];

  getData(){
    return this.http
    .get('http://localhost:8000/api/event')
    .map(result => result.json())
    .catch(error => Observable.throw(error.json().error) || "Server Error");
  }


  buyTicket(obj: Object){

    let body = JSON.stringify(obj); 
    let headers = new Headers({ "Content-Type" : "application/json" });
    let options = new RequestOptions({ headers : headers });

    return this.http.post('http://localhost:8000/api/event/buy', body, options)
    .map(result => result.json());

  }

  addData(obj: Object){

    let body = JSON.stringify(obj); 
    let headers = new Headers({ "Content-Type" : "application/json" });
    let options = new RequestOptions({ headers : headers });

    return this.http.post('http://localhost:8000/api/event/add', body, options)
    .map(result => result.json());

  }

  getDetail(obj: Object){

    let body = JSON.stringify(obj); 
    let headers = new Headers({ "Content-Type" : "application/json" });
    let options = new RequestOptions({ headers : headers });

    return this.http
    .post('http://localhost:8000/api/event/detail', body, options)
    .map(result => result.json());
  }

}

import { LatihanPage } from './app.po';

describe('latihan App', () => {
  let page: LatihanPage;

  beforeEach(() => {
    page = new LatihanPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
